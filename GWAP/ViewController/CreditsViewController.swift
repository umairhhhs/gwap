//
//  CreditsViewController.swift
//  GWAP
//
//  Created by Umair on 09.12.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import UIKit

class CreditsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func logOutAction(_ sender: Any) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey:"player")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
