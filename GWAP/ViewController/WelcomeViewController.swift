//
//  WelcomeViewController.swift
//  GWAP
//
//  Created by Umair on 08.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import Moya_ModelMapper

class WelcomeViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    @IBOutlet var signinButton: UIButton!
    
    @IBOutlet var signupButton: UIButton!
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signinButton.tag = 0
        signupButton.tag = 1
        if let Icon = UIImage(named: "User Icon"){
            usernameTextField.withImage(direction: .Left, image: Icon, colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
        }
        if let pass = UIImage(named: "Password icon"){
            passwordTextField.withImage(direction: .Left, image: pass, colorSeparator: UIColor.clear, colorBorder: UIColor.clear)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Do any additional setup after loading the view.
        usernameTextField.delegate = self
        passwordTextField.delegate = self
        passwordTextField.isSecureTextEntry = true
//        passwordTextField.textContentType = .oneTimeCode
//        usernameTextField.textContentType = .oneTimeCode
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height - 60
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height - 60
            }
        }
    }
    @IBAction func signinAction(_ sender: Any) {
        NetworkAdapter.request(target: .user_login(username: usernameTextField.text!, password: passwordTextField.text!))
            .map(to: Player.self)
            .subscribe(onNext: { (Response) in
                let Player = Response
                
                let encoder = JSONEncoder()
                if let encoded = try? encoder.encode(Player) {
                    let defaults = UserDefaults.standard
                    defaults.set(encoded, forKey: "player")
                }
                self.performSegue(withIdentifier: "signIn", sender: sender)
                print("This is from NEXT: \(Response)")
                
            }, onError: { (error: Error) in
                let moyaError: MoyaError? = error as? MoyaError
                
                let response : Response? = moyaError?.response
                
                let statusCode : Int? = response?.statusCode
                print("moyaError is : \(String(describing: moyaError)), response is : \(response!.data.toString()), statusCode is : \(String(describing: statusCode))")
                
                let alert = UIAlertController(title: "Error", message: response!.data.toString(), preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                self.present(alert, animated: true)
                
            }).disposed(by: disposeBag)
    }
    
    @IBAction func signupAction(_ sender: Any) {
        self.performSegue(withIdentifier: "signUp", sender: sender)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.view.endEditing(true)

//        if let button = sender as? UIButton {
//            if button.tag == 0{
//
//
//            }else{
//
//            }
//        }
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
    }
    
    
}
extension Data
{
    func toString() -> String
    {
        return String(data: self, encoding: .utf8)!
    }
}
