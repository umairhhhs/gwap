//
//  MainMenuViewController.swift
//  GWAP
//
//  Created by Umair on 08.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import UIKit


class MainMenuViewController: UIViewController {
    public var gameSelected:Int?
//    @IBOutlet var profileButton: UIBarButtonItem!{
//        didSet {
//            let imageSetting = UIImageView(image: UIImage(named: "Mask"))
//            imageSetting.image = imageSetting.image!.withRenderingMode(.alwaysOriginal)
//            imageSetting.tintColor = UIColor.clear
//            profileButton.image = imageSetting.image
//        }
//    }
    @IBOutlet var demoButton: UIButton!
    @IBOutlet var startButton: UIButton!
    @IBOutlet var settingsButton: UIButton!
    @IBOutlet var creditsButton: UIButton!
    var identifier: String?
    var gameOnFlag: Bool = true
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        gameOnFlag=true
        super.viewWillAppear(animated)
        demoButton.tag = 0
        switch gameSelected {
        case 0:
            identifier  = "tweeterGame"
        case 1:
            identifier  = "rhetoricalFigures"
        case .none:
            print("")
        case .some(_):
            print("")
            
        }
    }
    @IBAction func demoButtonAction(_ sender: Any) {
        self.performSegue(withIdentifier: identifier!, sender: self)
    }
    @IBAction func startButtonAction(_ sender: Any) {
        gameOnFlag = false
        self.performSegue(withIdentifier: identifier!, sender: self)
        
    }
    @IBAction func settingsButtonAction(_ sender: Any) {
        gameOnFlag = false
        self.performSegue(withIdentifier: "settings", sender: self)
    }
    @IBAction func creditsButtonAction(_ sender: Any) {
        gameOnFlag = false
        self.performSegue(withIdentifier: "credits", sender: self)
    }
    @IBAction func logOutAction(_ sender: Any) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey:"player")
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if gameOnFlag {
            
                switch gameSelected {
                case 0:
                    let vc = segue.destination as! GuessTweetViewController
                    vc.isDemoNeeded = true
                case 1:
                    let vc = segue.destination as! RhetoricalFiguresViewController
                    vc.isDemoNeeded = true
               
                    
                case .none:
                    print("")

                case .some(_):
                    print("")

                }
            
        
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    }

}
