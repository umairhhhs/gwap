//
//  GameListViewController.swift
//  GWAP
//
//  Created by Umair on 10.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import UIKit
import Moya
import RxSwift


class GameListViewController: UIViewController {
    var activePlayer: Player?
    var userImage: UIImage?
    let bag = DisposeBag()
    @IBOutlet var profileButton: UIBarButtonItem!{
        didSet {
            let imageSetting = UIImageView(image: UIImage(named: "Mask"))
            imageSetting.image = imageSetting.image!.withRenderingMode(.alwaysOriginal)
            imageSetting.tintColor = UIColor.clear
            profileButton.image = imageSetting.image
        }
    }
    @IBOutlet var guessTweetButton: UIButton!
    @IBOutlet var rhetoricalFiguresButton: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let defaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "player") as? Data {
            let decoder = JSONDecoder()
            activePlayer = try? decoder.decode(Player.self, from: savedPerson)
        }
        
            if userImage != nil{
                var image = userImage!.convert(toSize:CGSize(width:50, height:50), scale: UIScreen.main.scale)
                image = image.roundedImage
                let imageSetting = UIImageView(image: image)
                imageSetting.image = imageSetting.image!.withRenderingMode(.alwaysOriginal)
                imageSetting.tintColor = UIColor.clear
                profileButton.image = imageSetting.image
                

                NetworkAdapter.request(target: .user_login(username: (activePlayer?.username)!, password: (activePlayer?.password)!))
                    .map(to: Player.self)
                    .subscribe(onNext: { (Response) in
                        let Player = Response
                        
                        let encoder = JSONEncoder()
                        if let encoded = try? encoder.encode(Player) {
                            let defaults = UserDefaults.standard
                            defaults.set(encoded, forKey: "player")
                        }
                        
                    }, onError: { (error: Error) in
                        
                    }).disposed(by: bag)
                
            
            } else {
            // the value of someOptional is not set (or nil).
    if activePlayer!.photoid != nil {
            NetworkAdapter.request(target: .get_image(id: activePlayer!.photoid!))
                .subscribe(onNext: { (Response) in
                    print(String(data: Response.data, encoding: .utf8)!.imageFromBase64)
                    self.userImage = try? String(data: Response.data, encoding: .utf8)!.imageFromBase64
                    self.userImage = self.userImage!.convert(toSize:CGSize(width:50, height:50), scale: UIScreen.main.scale)
                    let imageSetting = UIImageView(image: self.userImage)
                    imageSetting.image = imageSetting.image!.withRenderingMode(.alwaysOriginal)
                    imageSetting.tintColor = UIColor.clear
                    self.profileButton.image = imageSetting.image
                    
                }, onError: { (error: Error) in
                    
                }).disposed(by: bag)
        }
        }
    }
    @IBAction func logOutAction(_ sender: Any) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey:"player")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "player") as? Data {
            let decoder = JSONDecoder()
            activePlayer = try? decoder.decode(Player.self, from: savedPerson)
        }
        guessTweetButton.tag = 0
        rhetoricalFiguresButton.tag = 1
        // Do any additional setup after loading the view.
    }
    @IBAction func guessTweetAction(_ sender: Any) {

    }

    @IBAction func rehtoricalFiguresAction(_ sender: Any) {

    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let button = sender as? UIButton {
        let vc = segue.destination as! MainMenuViewController
        vc.gameSelected = button.tag
        }
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}
