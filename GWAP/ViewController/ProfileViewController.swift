//
//  ProfileViewController.swift
//  GWAP
//
//  Created by Umair on 08.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import UIKit
import RxSwift
import Moya_ModelMapper
import Moya
import NVActivityIndicatorView
import Kingfisher
import RxCocoa




class ProfileViewController: UIViewController {
    
    var activePlayer: Player?
    var bag = DisposeBag()
    
    let networkPlugin = NetworkActivityPlugin { type , call  in
        let activityData = ActivityData()
        if call.path != "/images/getphoto" {
        switch type {
        case .began:
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        case .ended:
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
        }
        }
    }
    
    @IBOutlet var profileCell: UITableViewCell!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var rankAndPoints: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var rating: UILabel!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var questionNumbersLabel: UILabel!
    @IBOutlet var answerNumbersLabel: UILabel!
    @IBOutlet var thumbsNumbersLabel: UILabel!
    
    @IBAction func logOutAction(_ sender: Any) {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey:"player")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.contentInset = UIEdgeInsets(top: 80, left: 0, bottom: 0, right: 0)
        
        let defaults = UserDefaults.standard
        if let savedPerson = defaults.object(forKey: "player") as? Data {
        let decoder = JSONDecoder()
        activePlayer = try? decoder.decode(Player.self, from: savedPerson)
        }
        NetworkAdapter.provider = MoyaProvider<Backend>(plugins:[networkPlugin])
        NetworkAdapter.request(target: .get_profile(userid: activePlayer!.id))
            .map(to: [Profile].self)
            .subscribe(onNext: { (Response) in
                let userProfile = Response
                self.setProfile(profile: userProfile[0])
            print("Response is = \(userProfile)")
            }, onError: { (error: Error) in
            let moyaError: MoyaError? = error as? MoyaError
            
            let response : Response? = moyaError?.response
            
            let statusCode : Int? = response?.statusCode
            
            let message = String(data: (response?.data)!, encoding: .utf8)
                
            print("\n\n\n moyaError is : \(String(describing: moyaError)), \n\n\n response is : \(response!.data.toString()),\n\n\n statusCode is : \(String(describing: statusCode)),\n\n\n Message is : \(String(describing: message))")
            }).disposed(by: bag)
        
        NetworkAdapter.request(target: .get_profiles)
            .map(to: [Profile].self)
            .subscribe(onNext: { (Response) in
                let userProfile = Response
                let items = Observable.just(userProfile)
                items.bind(to: self.tableView.rx.items(cellIdentifier: "ProfileCellTableViewCell", cellType: ProfileCellTableViewCell.self)) { (row, element, cell) in
                    
                    cell.username?.text = "\(element.username) "
                    cell.questions?.text = "\(element.questions)"
                    cell.answers?.text = "\(element.answers) "
                    cell.ranking?.text = "\(element.rank) "
                    if element.photoid != nil{
                        self.setProfilePicForCell(cell: cell, photoId: element.photoid!)
//                        self.bag = cell.disposeBag
                    }
                    }
                    .disposed(by: self.bag)
//                print("Response is tableview= \(userProfile)")
            }, onError: { (error: Error) in
                let moyaError: MoyaError? = error as? MoyaError
                
                let response : Response? = moyaError?.response
                
                let statusCode : Int? = response?.statusCode
                
                let message = String(data: (response?.data)!, encoding: .utf8)
                
                print("\n\n\n moyaError is : \(String(describing: moyaError)), \n\n\n response is : \(response!.data.toString()),\n\n\n statusCode is : \(String(describing: statusCode)),\n\n\n Message is : \(String(describing: message))")
            }).disposed(by: bag)

//        let items = Observable.just(
//            (0..<20).map { "\($0)" }
//        )
//        
//        items
//            .bind(to: tableView.rx.items(cellIdentifier: "Cell", cellType: UITableViewCell.self)) { (row, element, cell) in
//                cell.textLabel?.text = "\(element) @ row \(row)"
//            }
//            .disposed(by: bag)
        
        // Do any additional setup after loading the view.
    }
    
    func setProfilePicForCell(cell: ProfileCellTableViewCell, photoId: String) {
        
        NetworkAdapter.request(target: .get_image(id: photoId))
            .subscribe(onNext: { (Response) in
                print(Response.data)
                cell.profilePic.image = try? String(data: Response.data, encoding: .utf8)!.imageFromBase64
                
            }, onError: { (error: Error) in
                
            }).disposed(by: bag)
    }
    
    func setProfile(profile: Profile) {
            rankAndPoints.text = String("Rank: \(profile.rank), Points: \(profile.points)")
            userName.text = profile.username
            rating.text = String(profile.ratings)
//            profileImage.tex = profile.rank
            questionNumbersLabel.text = String(profile.rank)
            answerNumbersLabel.text = String(profile.rank)
            thumbsNumbersLabel.text = String(profile.thumbs)
//        let url = URL(string: "http://192.168.1.100:3000/api/images/getphoto?id=" + profile.photoid!)
//
//        self.profileImage.kf.setImage(with: url)
        NetworkAdapter.request(target: .get_image(id: profile.photoid!))
            .subscribe(onNext: { (Response) in
                print(Response.data)
                self.profileImage.image = try? String(data: Response.data, encoding: .utf8)!.imageFromBase64

            }, onError: { (error: Error) in
            
        }).disposed(by: bag)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

