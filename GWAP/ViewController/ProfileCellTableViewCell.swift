//
//  ProfileCellTableViewCell.swift
//  GWAP
//
//  Created by Umair on 13.02.19.
//  Copyright © 2019 Continental. All rights reserved.
//

import UIKit
import RxSwift

class ProfileCellTableViewCell: UITableViewCell {

    @IBOutlet var username: UILabel!
    @IBOutlet var questions: UILabel!
    @IBOutlet var answers: UILabel!
    @IBOutlet var ranking: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    var disposeBag = DisposeBag()
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profilePic.image = UIImage(named: "Mask")
        
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
        // Set cell to initial state here, reset or set values
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
