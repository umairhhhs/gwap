//
//  SignUpViewController.swift
//  GWAP
//
//  Created by Umair on 08.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import Moya_ModelMapper
import SwiftyJSON
import NVActivityIndicatorView

class SignUpViewController: UIViewController, UITextFieldDelegate {

    let networkPlugin = NetworkActivityPlugin { type , call  in
        let activityData = ActivityData()
        switch type {
        case .began:
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        case .ended:
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
            
        }
    }

    
    @IBOutlet var firstNameField: UITextField!
    @IBOutlet var lastNameField: UITextField!
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var repeatPasswordField: UITextField!
    var email:String?
    let disposeBag = DisposeBag()
//    @IBOutlet var profilePicButton: UIButton!
    @IBOutlet var signupButton: UIButton!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Do any additional setup after loading the view.
        firstNameField.delegate = self
        lastNameField.delegate = self
        usernameField.delegate = self
        passwordField.delegate = self
        repeatPasswordField.delegate = self
        passwordField.isSecureTextEntry = true
        repeatPasswordField.isSecureTextEntry = true

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height - 100
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height - 100
            }
        }
    }
    
    @IBAction func profileButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func signupbuttonAction(_ sender: Any) {
        NetworkAdapter.provider = MoyaProvider<Backend>(plugins:[networkPlugin])
        NetworkAdapter.request(target: .user_signup(firstname: firstNameField.text!, lastname: lastNameField.text!, username: usernameField.text!, email: email!, password: passwordField.text!))
            .map(to: Player.self)
            .subscribe(onNext: { (Response) in
            let Player = Response
            
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(Player) {
                let defaults = UserDefaults.standard
                defaults.set(encoded, forKey: "player")
            }
            self.performSegue(withIdentifier: "profilePhoto", sender: sender)
            print("This is from NEXT: \(Response)")
            
        }, onError: { (error: Error) in
            let moyaError: MoyaError? = error as? MoyaError
            
            let response : Response? = moyaError?.response
            
//            let statusCode : Int? = response?.statusCode
//            print("\n\n\n moyaError is : \(String(describing: moyaError)), \n\n\n response is : \(response!.data.toString()),\n\n\n statusCode is : \(String(describing: statusCode))")
           var json = JSON()
            do {
                 json = try JSON(data: response!.data)
                print(json)
            } catch _ {
            }
            let alert = UIAlertController(title: json["_message"].stringValue, message: json["message"].stringValue, preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            self.present(alert, animated: true)
            
        }).disposed(by: disposeBag)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.view.endEditing(true)
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
