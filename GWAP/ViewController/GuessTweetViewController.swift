//
//  GuessTweetViewController.swift
//  GWAP
//
//  Created by Umair on 08.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import UIKit
import RxSwift
import Moya
import Moya_ModelMapper
import NVActivityIndicatorView
import CountdownLabel

class GuessTweetViewController: UIViewController,UIPageViewControllerDelegate {

    @IBOutlet var demo: UIView!
    
    @IBAction func skipTour(_ sender: Any) {
        isDemoNeeded = false
        self.viewWillAppear(true)
    }
    @IBAction func startAgain(_ sender: Any) {
        let pager = PageViewController()
        pager.delegate   = self
        pager.viewDidLoad()
    }
    @IBOutlet var bigLikeV: UIImageView!
    @IBOutlet weak var counter: CountdownLabel!
    let networkPlugin = NetworkActivityPlugin { type , call  in
        let activityData = ActivityData()
            switch type {
            case .began:
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
            case .ended:
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
                
            }
    }
    let disposeBag = DisposeBag()
    var player: Player?
    var tweets: [Tweet]?
    var currentTweet = 0
    public var isDemoNeeded: Bool?
    @IBOutlet var option1: UIButton!
    @IBOutlet var option2: UIButton!
    @IBOutlet var option3: UIButton!
    @IBOutlet var option4: UIButton!

    @IBOutlet var dateAndTimeLabel: UILabel!
    @IBOutlet var tweetTextLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isDemoNeeded == true {
            demo.isHidden = false
            self.title  = "Demo"
        }else{
        demo.isHidden = true
        self.title  = "Guess Tweet"
        let defaults = UserDefaults.standard
        let decoder = JSONDecoder()
        player = try! decoder.decode(Player.self, from:defaults.object(forKey: "player") as! Data)
        // Do any additional setup after loading the view.
        counter.countdownDelegate = self
        counter.setCountDownTime(minutes: 30)
        counter.text = "Count Down:  HERE"
        counter.timeFormat = "ss"
        counter.countdownAttributedText = CountdownAttributedText(text: "Count Down:  HERE", replacement: "HERE")
        fetchTweets()
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func fetchTweets()  {
        NetworkAdapter.provider = MoyaProvider<Backend>(plugins:[networkPlugin])
        NetworkAdapter.request(target: .get_tweets(userid: player!.id, level: "easy", seen: []))
            .map(to: [Tweet].self)
            .subscribe(onNext: { (Response) in
                self.tweets = Response
                print(self.tweets as Any)
            }, onError: { (Error) in
            }, onCompleted: {
                self.updateUI()
            }).disposed(by: disposeBag)
    }
   
    func updateUI() {
        self.option1.setTitle(self.tweets?[0].options[0], for: .normal)
        self.option2.setTitle(self.tweets?[0].options[1], for: .normal)
        let filtered = self.tweets?[0].text.replacingOccurrences(of: "&amp;", with: "&")
        self.tweetTextLabel.text = filtered
        self.tweetTextLabel.sizeToFit()
        counter.start()
        
    }
    
    @IBAction func option1ButtonAction(_ sender: Any) {
        if ((self.tweets?[currentTweet].handle) == "Donald Trump") {
            resultAnimation(correct: true)
        }
        else{
            resultAnimation(correct: false)
        }
        
        anotateTweets(option: (self.tweets?[currentTweet].options[0])!, userID: player!.id, tweetID: (self.tweets?[currentTweet].id)!)
        if currentTweet + 1 != self.tweets?.count{

        currentTweet += 1
        let filtered = self.tweets?[currentTweet].text.replacingOccurrences(of: "&amp;", with: "&")
        self.tweetTextLabel.text = filtered
        self.tweetTextLabel.sizeToFit()
        self.startCounter()
        }else{
        currentTweet = 0
        fetchTweets()
        }

    }
    
    @IBAction func option2ButtonAction(_ sender: Any) {
        if ((self.tweets?[currentTweet].handle) == "Hillary Clinton") {
            resultAnimation(correct: true)
        }
        else{
            resultAnimation(correct: false)
        }
        
        anotateTweets(option: (self.tweets?[currentTweet].options[1])!, userID: player!.id, tweetID: (self.tweets?[currentTweet].id)!)
        if currentTweet + 1 != self.tweets?.count{

        currentTweet += 1
        let filtered = self.tweets?[currentTweet].text.replacingOccurrences(of: "&amp;", with: "&")
        self.tweetTextLabel.text = filtered
        self.tweetTextLabel.sizeToFit()
        self.startCounter()
        }else{
            currentTweet = 0
            fetchTweets()
        }

    }
    
    @IBAction func option3ButtonAction(_ sender: Any) {
        if currentTweet + 1 != self.tweets?.count{

        currentTweet += 1
        let filtered = self.tweets?[currentTweet].text.replacingOccurrences(of: "&amp;", with: "&")
        self.tweetTextLabel.text = filtered
        self.tweetTextLabel.sizeToFit()
        self.startCounter()
        }else{
        currentTweet = 0
        fetchTweets()
        }

    }
    
    @IBAction func option4ButtonAction(_ sender: Any) {
        if currentTweet + 1 != self.tweets?.count{

        currentTweet += 1
        let filtered = self.tweets?[currentTweet].text.replacingOccurrences(of: "&amp;", with: "&")
        self.tweetTextLabel.text = filtered
        self.tweetTextLabel.sizeToFit()
        self.startCounter()

        }else{
            currentTweet = 0
            fetchTweets()
        }

    }
    
    func anotateTweets(option: String, userID: String, tweetID: String)  {
        NetworkAdapter.provider = MoyaProvider<Backend>(plugins:[networkPlugin])
        NetworkAdapter.request(target: .post_tweets(userid: userID, tweetid: tweetID, optionselected: option))
//            .map(to: [Tweets].self)
            .subscribe(onNext: { (Response) in
//                self.tweets = Response
                
//                print("This is from NEXT: \(String(describing: String(data: Response.data, encoding: .utf8) ))")
            }, onError: { (Error) in
//                let errorString = String(describing: Error.localizedDescription)
//                print("This is from ERROR: \(errorString)")
            }, onCompleted: {
                print("completed")
//                self.updateUI()
            }).disposed(by: disposeBag)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func startCounter()  {
        counter.setCountDownTime(minutes: 30)
        counter.text = "Count Down:  HERE"
        counter.timeFormat = "ss"
        counter.countdownAttributedText = CountdownAttributedText(text: "Count Down:  HERE", replacement: "HERE")
        counter.start()
    }
    func resultAnimation(correct: Bool) {
        if correct {
            bigLikeV.image = UIImage(named: "right")
        }
        else{
            bigLikeV.image = UIImage(named: "wrong")
        }
        if let bigLikeImageV = bigLikeV {
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.2, options: .allowUserInteraction, animations: {
                bigLikeImageV.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
                bigLikeImageV.alpha = 1.0
            }) { finished in
                bigLikeImageV.alpha = 0.0
                bigLikeImageV.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                
                let alert = UIAlertController(title: "Done", message: "Next tweet is ready for you.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in self.counter.start() }))
                self.counter.pause()
                self.present(alert, animated: true)
            }
        }
    }
}

extension GuessTweetViewController: CountdownLabelDelegate {
    func countdownFinished() {
//        debugPrint("countdownFinished at delegate.")
        
        
    }
    
    func countingAt(timeCounted: TimeInterval, timeRemaining: TimeInterval) {
        switch timeRemaining {
        case 1.0:
            self.option3ButtonAction((Any).self)
        default:
            print("Time Remaining: \(timeRemaining)")
        }

    }
    
}
