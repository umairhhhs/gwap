//
//  UploadPhotoViewController.swift
//  GWAP
//
//  Created by Umair on 13.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import UIKit
import CropViewController
import RxSwift
import Moya_ModelMapper
import Moya
import NVActivityIndicatorView


class UploadPhotoViewController: UIViewController,UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    var activePlayer: Player?
    let bag = DisposeBag()

    let networkPlugin = NetworkActivityPlugin { type , call  in
        let activityData = ActivityData()
        switch type {
        case .began:
            NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData, nil)
        case .ended:
            NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
        }
    }
    
    var profileImage: UIImage?
    @IBOutlet var cameraButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func finishButtonAction(_ sender: Any) {
        if profileImage != nil {
            let base64String = profileImage?.base64(format: ImageFormat.png)
            let defaults = UserDefaults.standard
            if let savedPerson = defaults.object(forKey: "player") as? Data {
                let decoder = JSONDecoder()
                activePlayer = try? decoder.decode(Player.self, from: savedPerson)
            }
            NetworkAdapter.provider = MoyaProvider<Backend>(plugins:[networkPlugin])
            NetworkAdapter.request(target: .post_image(photo: base64String!, userid: activePlayer!.id))
                .subscribe(onNext: { (Response) in
                    self.performSegue(withIdentifier: "finish", sender: self)
                
            }, onError: { (Error) in
                let moyaError: MoyaError? = Error as? MoyaError
                
                let response : Response? = moyaError?.response
                
                let statusCode : Int? = response?.statusCode
                print(response)
            }).disposed(by: bag)
        }else{
            
        }
    }
    @IBAction func cameraButtonAction(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        // print out the image size as a test
//        var newImage = image.convert(toSize:CGSize(width:62.0, height:62), scale: UIScreen.main.scale)
        
        var newImage = image.convert(toSize: CGSize(width: 200, height: 200), scale: UIScreen.main.scale)
        newImage = newImage.roundedImage
        cameraButton.setImage(newImage, for: UIControl.State.normal)
        profileImage  = newImage
        print(newImage.size)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let nav = segue.destination as! UINavigationController
        let vc = nav.topViewController as! GameListViewController
        vc.userImage = profileImage
    }

}
