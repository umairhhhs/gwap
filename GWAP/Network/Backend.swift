//
//  Backend.swift
//  PlatooningService
//
//  Created by Umair on 31.10.18.
//  Copyright © 2018 Continental Automotive GmbH. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import Moya_ModelMapper

public enum Backend {

    
    case user_login(username: String, password: String)
    case user_signup(firstname:String, lastname:String, username:String, email: String, password: String)
    case post_image(photo: String, userid: String)
    case get_profile(userid: String)
    case get_profiles
    case get_tweets(userid: String, level: String, seen: [String])
    case post_tweets(userid:String, tweetid:String, optionselected:String)
    case get_statements(userid: String, level: String, seen: [String])
    case post_statements(userid:String, statementid:String, optionselected:String)
    case get_image(id: String)
}
extension Backend: TargetType {
    // 1
    public var baseURL: URL {
        let modelName = UIDevice.modelName
        if modelName.range(of:"Simulator") == nil {
            return URL(string: "http://192.168.2.130:3000/api")!
        }
        else{
        return URL(string: "http://localhost:3000/api")!
        }
        
    }
    
    // 2
    public var path: String {
        switch self {
        case .user_login: return "/users/login"
        case .user_signup: return "/users/signup"
        case .post_image: return "/images/photo"
        case .get_image: return "/images/getphoto"
        case .get_tweets: return "/tweets/gettweets"
        case .post_tweets: return "/tweets/annotate"
        case .get_statements: return "/statements/getstatements"
        case .post_statements: return "/statements/annotate"
        case .get_profile: return "/profiles"
        case .get_profiles: return "/profiles"
        }
    }
    
    // 3
    public var method: Moya.Method {
        switch self {
        case .user_login: return .post
        case .user_signup: return .post
        case .post_image: return .post
        case .get_image: return .get
        case .get_tweets: return .get
        case .post_tweets: return .post
        case .get_statements: return .get
        case .post_statements: return .post
        case .get_profile: return .get
        case .get_profiles: return .get
        }
    }
    
    // 4
    public var sampleData: Data {
        return Data()
    }
    
    // 5
    public var task: Task {
        //"message": "Missing Authentication Token"
        // need auth Api details to implementation
        let encoding: ParameterEncoding
        switch self.method {
        case .post:
            encoding = JSONEncoding.default
        default:
            encoding = URLEncoding.default
        }
        
        if let requestParameters = parameters {
            return .requestParameters(parameters: requestParameters, encoding: encoding)
        }
        return .requestPlain
        
    }
    
    // 6
    public var headers: [String: String]? {
        return ["Content-Type": "application/json"]
    }
    public var parameters: [String : Any]? {
        switch self {
        case .user_login(let username, let password):
            return ["username": username, "password": password]

        case .user_signup(let firstname, let lastname, let username, let email, let password):
             return ["username": username, "password": password, "firstname": firstname, "lastname": lastname, "email": email]
        case .get_tweets(let userid, let level, let seen):
            return [ "id": userid , "level": level, "seen": seen ]
        case .post_tweets(let userid, let tweetid, let optionselected):
            return ["userid": userid, "tweetid": tweetid, "optionselected": optionselected ]
        case .get_statements(let userid, let level, let seen):
            return [ "id": userid , "level": level, "seen": seen ]
        case .post_statements(let userid, let statementid, let optionselected):
            return ["userid": userid, "statementid": statementid, "optionselected": optionselected ]
        case .get_image( let id):
            return ["id": id]
        case .post_image(let photo, let userid):
            return [ "photo": photo, "userid": userid]
        case .get_profile(let userid):
            return ["userid": userid]
        case .get_profiles:
            return ["sort":"rank"]
        }
    }
    // 7
//    public var validationType: ValidationType {
//        return .successCodes
//    }


}
struct NetworkAdapter {
    
    static var provider = MoyaProvider<Backend>()
    
    static func request(target: Backend) -> Observable<Response> {
        
        return provider.rx.request(target)
        .asObservable()
        .filterSuccessfulStatusCodes()
        
        //        provider.request(target) { (result) in
//            switch result {
//            case .success(let response):
//                // 1:
//                if response.statusCode >= 200 && response.statusCode <= 300 {
//                    successCallback(response)
//                } else {
//                    // 2:
//                    let error = NSError(domain:String(data: response.data, encoding: .utf8)! , code:response.statusCode, userInfo:[NSLocalizedDescriptionKey: response.response.debugDescription])
//                    errorCallback(error)
//                }
//            case .failure(let error):
//                // 3:
//                failureCallback(error)
//            }
        
    }
}
