//
//  SUNetworkActivityIndicator.swift
//  GWAP
//
//  Created by Umair on 06.12.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import Foundation
import UIKit

/**
 Notification name for `activeCount` changed.
 Contains `activeCount` in notification.object as `Int`
 */
public let NetworkActivityIndicatorActiveCountChangedNotification = "ActiveCountChanged"

/// NetworkActivityIndicator
public class NetworkActivityIndicator {
    
    /// Singleton: shared instance (private var)
    private static let sharedInstance = NetworkActivityIndicator()
    
    /// enable sync access (like Objective-C's `@synchronized`)
    private let syncQueue = DispatchQueue(label: "NetworkActivityIndicatorManager.syncQueue",
                      attributes:.init() )
    
    /**
     ActiveCount
     
     If count is above 0,
     
     ```
     UIApplication.sharedApplication().networkActivityIndicatorVisible
     ```
     
     is true, else false.
     */
    private(set) public var activeCount: Int {
        didSet {
            UIApplication.shared
                .isNetworkActivityIndicatorVisible = activeCount > 0
            if activeCount < 0 {
                activeCount = 0
            }
            NotificationCenter.default.post(
                name: NSNotification.Name(rawValue: NetworkActivityIndicatorActiveCountChangedNotification),
                object: activeCount
            )
        }
    }
    
    /**
     Initializer (private)
     
     - returns: instance
     */
    private init() {
        self.activeCount = 0
    }
    
    public class func shared() -> NetworkActivityIndicator {
        return sharedInstance
    }
    
    /**
     Count up `activeCount` and `networkActivityIndicatorVisible` change to true.
     */
    public func start() {
        syncQueue.sync() { [unowned self] in
            self.activeCount += 1
        }
    }
    
    /**
     Count down `activeCount` and if count is zero, `networkActivityIndicatorVisible` change to false.
     */
    public func end() {
        syncQueue.sync() { [unowned self] in
            self.activeCount -= 1
        }
    }
    
    /**
     Reset `activeCount` and `networkActivityIndicatorVisible` change to false.
     */
    public func endAll() {
        syncQueue.sync() { [unowned self] in
            self.activeCount = 0
        }
    }
    
}
