//
//  Tweets.swift
//  GWAP
//
//  Created by Umair on 14.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import Foundation
import Mapper

struct Image: Mappable {
    let id: String?
    let photo: String
    let userid: String?
    //
    
    //    let url: String? // Optional property
    
    init(map: Mapper) throws {
        try id = map.optionalFrom("_id")
        try photo = map.from("photo")
        try userid = map.optionalFrom("userid")
        //        url = map.optionalFrom("url") // Optional property
    }
    
}
