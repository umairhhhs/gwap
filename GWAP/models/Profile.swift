//
//  player.swift
//  GWAP
//
//  Created by Umair on 13.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import Foundation
import Mapper

struct Profile: Mappable, Codable {
    let id: String
    let username: String
    let userid:  String
    let questions:  Int
    let answers:  Int
    let thumbs:  Int
    let rank:  Int
    let points:  Int
    let ratings:  Int
    var photoid:  String?
    
    //    let url: String? // Optional property
    
    init(map: Mapper) throws {
        try id = map.from("_id")
        try username = map.from("username")
        try userid = map.from("userid")
        try questions = map.from("questions")
        try answers = map.from("answers")
        try thumbs = map.from("thumbs")
        try rank = map.from("rank")
        try points = map.from("points")
        try ratings = map.from("ratings")
        photoid = map.optionalFrom("photoid") // Optional property
    }
    
}
