//
//  player.swift
//  GWAP
//
//  Created by Umair on 13.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import Foundation
import Mapper

struct Player: Mappable, Codable {
    let id: String
    let firstname: String
    let lastname: String
    let username: String
    let password: String
    let email: String
    let photoid: String?
    
//    let url: String? // Optional property
    
    init(map: Mapper) throws {
        try id = map.from("_id")
        try firstname = map.from("firstname")
        try lastname = map.from("lastname")
        try username = map.from("username")
        try password = map.from("password")
        try email = map.from("email")
        try photoid = map.optionalFrom("photoid")
//        url = map.optionalFrom("url") // Optional property
    }
    
}
