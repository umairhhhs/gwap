//
//  Tweets.swift
//  GWAP
//
//  Created by Umair on 14.11.18.
//  Copyright © 2018 Continental. All rights reserved.
//

import Foundation
import Mapper

struct Tweet: Mappable {
    let id: String
    let time: String
    let text: String
    let handle: String
    let options: [String]
    let seenby: [String]
    let level: String
//   
    
    //    let url: String? // Optional property
    
    init(map: Mapper) throws {
        try id = map.from("_id")
        try time = map.from("time")
        try text = map.from("text")
        try handle = map.from("handle")
        try options = map.from("options")
        try seenby = map.from("seenby")
        try level = map.from("level")
        //        url = map.optionalFrom("url") // Optional property
    }
    
}
